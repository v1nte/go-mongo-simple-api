FROM golang:1.22-alpine AS builder

WORKDIR /app

RUN apk add --no-cache upx

COPY go.sum go.mod ./

RUN go mod download

COPY . .

RUN go build -ldflags "-s -w" -o /app_bin main.go
RUN upx --brute /app_bin

FROM scratch

COPY --from=builder /app_bin /app_bin

EXPOSE 3000

CMD ["/app_bin"]
