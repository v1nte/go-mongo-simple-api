package configs

import "github.com/gofiber/fiber/v2"

func FiberConfig() fiber.Config {

	return fiber.Config{
		CaseSensitive: false,
		AppName:       "Simple Go Fiber API",
	}

}
