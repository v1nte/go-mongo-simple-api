package handlers

import (
	"time"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/v1nte/go-mongo-simple-api/database"
	"gitlab.com/v1nte/go-mongo-simple-api/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func CreateSingleTask(ctx *fiber.Ctx) error {
	task := models.Task{}
	task.CreatedAt = time.Now()
	task.UpdatedAt = time.Now()

	if !ctx.Is("json") {
		return ctx.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"error": "The content isn't a JSON",
		})
	}

	if err := ctx.BodyParser(&task); err != nil {
		return err
	}

	if task.Name == "" {
		return ctx.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"error": "Missing fields...",
		})
	}

	task.ID = primitive.NewObjectID()

	if task.IsDone == nil {
		isDone := false
		task.IsDone = &isDone
	}

	_, err := database.Tasks.InsertOne(ctx.Context(), task)
	if err != nil {
		panic(err)
	}

	return ctx.Status(fiber.StatusCreated).JSON(fiber.Map{
		"info":   "Task created succesfully",
		"result": task,
	})
}

func GetTasks(ctx *fiber.Ctx) error {
	cursor, err := database.Tasks.Find(ctx.Context(), bson.M{})
	if err != nil {
		panic(err)
	}

	tasks := make([]models.Task, 0)

	if err = cursor.All(ctx.Context(), &tasks); err != nil {
		return ctx.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err})
	}
	return ctx.Status(fiber.StatusOK).JSON(tasks)
}

func UpdateSingleTaskById(ctx *fiber.Ctx) error {
	id, err := primitive.ObjectIDFromHex(ctx.Params("id"))
	if err != nil {
		return ctx.Status(fiber.StatusBadGateway).JSON(fiber.Map{
			"error": err,
		})
	}

	updatedTask := models.Task{}

	if err := ctx.BodyParser(&updatedTask); err != nil {
		return ctx.Status(fiber.StatusBadGateway).JSON(fiber.Map{
			"error": err,
		})
	}

	if updatedTask.Name == "" {
		return ctx.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"error": "missing 'name' field",
		})
	}

	isDonePresent := updatedTask.IsDone != nil

	updatedTask.UpdatedAt = time.Now()

	filter := bson.M{"_id": id}
	update := bson.M{"$set": bson.M{
		"name":       updatedTask.Name,
		"updated_at": updatedTask.UpdatedAt,
	}}

	if isDonePresent {
		update["$set"].(bson.M)["is_done"] = &updatedTask.IsDone
	}

	result, err := database.Tasks.UpdateOne(ctx.Context(), filter, update)
	if err != nil {
		return ctx.Status(403).JSON(fiber.Map{
			"id":    id,
			"error": err,
		})
	}

	return ctx.Status(fiber.StatusOK).JSON(fiber.Map{
		"info":   "updated",
		"result": result,
	})
}

func DeleteSingleTaskById(ctx *fiber.Ctx) error {
	id, err := primitive.ObjectIDFromHex(ctx.Params("id"))
	if err != nil {
		return ctx.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"error": "Bad id",
		})
	}

	filter := bson.M{"_id": id}

	result, err := database.Tasks.DeleteOne(ctx.Context(), filter)
	if err != nil {
		return ctx.Status(500).JSON(fiber.Map{
			"error": "opsi dopzy",
		})
	}
	if result.DeletedCount == 0 {
		return ctx.Status(fiber.StatusTeapot).JSON(fiber.Map{
			"info": "Nothing deleted",
		})
	}

	return ctx.Status(fiber.StatusOK).JSON(result)
}
