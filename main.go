package main

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/v1nte/go-mongo-simple-api/configs"
	"gitlab.com/v1nte/go-mongo-simple-api/middleware"
	"gitlab.com/v1nte/go-mongo-simple-api/routes"
	"gitlab.com/v1nte/go-mongo-simple-api/server"
)

func main() {
	config := configs.FiberConfig()

	app := fiber.New(config)

	middleware.FiberMiddleware(app)

	routes.PublicRoutes(app)

	server.StartServer(app)

}
