package models

type User struct {
	ID       string `json:"id"`
	Name     string `json:"name"`
	LastName string `json:"lastName"`
	Tasks    []Task
}

// var Users = []User{
// 	{
// 		ID:       uuid.NewString(),
// 		Name:     "Carlitos",
// 		LastName: "Marx",
// 		Tasks: []Task{
// 			{
// 				ID:     primitive.NewObjectID(),
// 				Name:   "jajaj",
// 				IsDone: bool,
// 			},
// 		},
// 	},
// }
