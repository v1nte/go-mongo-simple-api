package routes

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/v1nte/go-mongo-simple-api/handlers"
)

func PublicRoutes(app *fiber.App) {
	route := app.Group("/")

	route.Get("/tasks", handlers.GetTasks)
	route.Post("/task", handlers.CreateSingleTask)
	route.Delete("/task/:id", handlers.DeleteSingleTaskById)
	route.Patch("/task/:id", handlers.UpdateSingleTaskById)
}
