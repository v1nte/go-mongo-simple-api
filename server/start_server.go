package server

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/v1nte/go-mongo-simple-api/database"
)

func StartServer(app *fiber.App) {
	err := database.Init()
	if err != nil {
		panic(err)
	}

	defer func() {
		err := database.Close()
		if err != nil {
			panic(err)
		}
	}()

	if err := app.Listen(":3000"); err != nil {
		panic(err)
	}
}
